sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "sap/ui/model/Sorter",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
], function (Controller, JSONModel, MessageToast, MessageBox, Sorter, Filter, FilterOperator) {
    "use strict";

    return Controller.extend("sap.ui.putsykovich.alexei.controller.Start", {
        onInit : function () {

            var oViewModel = new JSONModel({pending: 0, accepted: 0, countAll: 0});
            this.getView().setModel(oViewModel, "orderListView");
            this._oTable = this.byId("orderTable");
            window.abc=this._oTable;
            this._mFilters = {
                "pending": [new  Filter("summary/status", FilterOperator.EQ,  "\'pending\'")],
                "accepted": [new Filter("summary/status", FilterOperator.EQ,   "\'accepted\'")],
                "all": []
            };
        },

        onQuickFilter: function(oEvent ) {
            var oBinding = this._oTable.getBinding("items"),
                sKey = oEvent.getParameter("selectedKey");

            oBinding.filter(this._mFilters[sKey]);
        },

        onUpdateFinished: function() {
            var oViewModel = this.getView().getModel("orderListView");
            var oODataModel = this.getView().getModel("odata");

            oODataModel.read("/Orders/$count", {
                success: function (oData) {
                    oViewModel.setProperty("/countAll", oData);
                }
            });
            oODataModel.read("/Orders/$count", {
                success: function (oData) {
                    oViewModel.setProperty("/pending", oData);
                },
                filters: this._mFilters.pending
            });
            oODataModel.read("/Orders/$count", {
                success: function(oData){
                    oViewModel.setProperty("/accepted", oData);
                },
                filters: this._mFilters.accepted
            });
        },

        onOpenOrderDialogPress:function () {
            var oOrderView = this.getView();
            var oODataModel = oOrderView.getModel("odata");
           if (!this.oOrderDialog) {
                this.oOrderDialog = sap.ui.xmlfragment(oOrderView.getId(), "sap.ui.putsykovich.alexei.view.fragments.OrderDialog", this);

                oOrderView.addDependent(this.oOrderDialog);
            }
            var ID = function () {
                return '_' + Math.random().toString(36).substr(2, 9);
            };
            var oOrderCtx=oODataModel.createEntry("/Orders",{
                properties:{
                    id:ID()
                }
            });
            window.trackEntry = oOrderCtx;
            this.oOrderDialog.setBindingContext(oOrderCtx);
            this.oOrderDialog.setModel(oODataModel);


            // open the dialog

            this.oOrderDialog.open();
        },

        onCancelPress:function () {
            var oODataModel = this.getView().getModel("odata");

            var oCtx = this.oOrderDialog.getBindingContext("odata");

            // delete the entry from requests queue
            oODataModel.deleteCreatedEntry(oCtx);
            this.oOrderDialog.close();
        },

        onCreateOrderPress:function (oEvent) {
            var oODataModel = this.getView().getModel("odata");
            oODataModel.submitChanges();
            this.oOrderDialog.close();
},

onOrderPress: function (oEvent) {
// get the source control of event object (the one that was fired event)
var oSource = oEvent.getSource();

// get the binding context of a button (it's a part of the table line, so it inherits the context of it)
var oCtx = oSource.getBindingContext("odata");


// get the component
var oComponent = this.getOwnerComponent();
oComponent.getRouter().navTo("secondPage", {
    orderId: oCtx.getObject("id")
});
},

onDeleteOrder: function (oEvent) {

var oCtx = oEvent.getParameter("listItem").getBindingContext("odata");
var oODataModel = oCtx.getModel();
var sKey = oODataModel.createKey("/Orders", oCtx.getObject());

oODataModel.remove(sKey, {
    success: function () {
        MessageToast.show("Orders was successfully removed!")
    },
    error: function () {
        MessageBox.error("Error while removing order!");
    }
});
}
});
});

